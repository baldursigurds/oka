cross(a,b) =
{
	[a[2]*b[3] - a[3]*b[2], \
	 a[3]*b[1] - a[1]*b[3], \
	 a[1]*b[2] - a[2]*b[1]];
}

bamboo(a,b) =
{
	local(w,i);
	cont = content(cross(a,b));
	q = 1;
	while(content(b+q*a) != cont, q++);
	r = cont/q;
	k = ncf(r);
	v = [a, (b+q*a)/cont];
	w = [(b+q*a)/cont];
	for(i=3, matsize(k)[2]+1,\
		v = concat(v, [k[i-2]*v[i-1] - v[i-2]]);
		w = concat(w, [v[i]]);
	);
	[k,w,cont/q];
}

/*
The follwoing function is an implementation of Oka's algorithm. It's input is
d  a matrix indicating the adjacancies of faces on the diagram
l  a list of list, listing the neighbouring noncompact faces
x  a list of the normal vectors to the faces of the diagram
The output is a list with two elements, the first is the intersection matrix,
the second is a list of all "normal vectors" extending the input x.
See examples below.
*/
Oka(d,l,x) =
{
	local(n,z,m,i,j,k,p,o,c);
	n = matsize(d)[1];
	z = n;
	m = matrix(n,n);
	for(i=1, n,
		for(j=i+1, n,\
			if(d[i,j],\
				o = bamboo(x[i],x[j]);
				if(o[3] == 1,\
					m[i,j] = 1;
					m[j,i] = 1;
				,
					c = block(o[3]);
					s = matsize(o[1])[2];
					m = matconcat([m,matrix(z,s);matrix(s,z),c]);
					m[i,z+1] = 1;
					m[z+1,i] = 1;
					z = z+s;
					m[z,j] = 1;
					m[j,z] = 1;
					x = concat(x, o[2]);
				);
			);
		);
	);
	for(i=1, n,\
		for(k=1, matsize(l[i])[2],\
			o = bamboo(x[i], l[i][k]);
			c = block(o[3]);
			s = matsize(c)[1];
			m = matconcat([m,matrix(z,s);matrix(s,z),c]);
			m[i,z+1] = 1;
			m[z+1,i] = 1;
			z = z+s;
			x = concat(x, o[2]);
			
		);
	);
	for(i=1, n,\
		for(k=1,3,\
			if(x[i][k] != 0,\
				p = 0;
				for(j=1, z,\
					if(j!=i,\
						p += x[j][k] * m[i,j];
					);
				);
				m[i,i] = -p/x[i][k];
			);
		);
	);
	[m,x];
}

block(r) =
{
	local(b,i);
	b = matdiagonal(-ncf(r));
	for(i=1,matsize(b)[1]-1,\
		b[i,i+1] = 1;
		b[i+1,i] = 1;
	);
	b;
}

/*
Zmin is a simple implementation of Laufer's celebrated algorithm to find Artin's
fundamental cycle.
*/
Zmin(m) =
{
	local(n,Z,i,dummy1, dummy2, p);
	n = matsize(m)[1];
	Z = vectorv(n, i, 1);
	dummy1 = 1;
	dummy2 = 0;
	while(dummy1,\
		dummy1 = 0;
		dummy2 = 1;
		p = m*Z;
		i=1;
		while(dummy2,\
			if(p[i] > 0,\
				Z[i] = Z[i] + 1;
				dummy1 = 1;
				dummy2 = 0;
			);
			if(i==n,\
				dummy2 = 0
			);
			i++;
		);
	);
	Z;
}

is_rat(m) =
{
	if(chi(Zmin(m)~,m) == 1,\
		return(1),\
		return(0)
	)
}

\\ represent a rational number as a negative continued fraction:
ncf(r) =
{
	local(k);
	k = [ceil(r)];
	while(r != ceil(r),\
		r = 1/(ceil(r)-r);
		k = concat(k,ceil(r));
	);
	k;
}

/*
Calculate the rational number given by the list k as a negative continued
fraction.
*/
incf(k) =
{
	local(s,r,i);
	s = matsize(k)[2];
	r = k[s];
	for(i=1, s-1,\
		r = k[s-i] - 1/r;
	);
	r;
}

vmin(k) =
{
	min(min(k[1],k[2]),k[3]);
}

Zmax(o) =
{
	vector(matsize(o[2])[2], i, vmin(o[2][i]));
}

\\ The canonical divisor given an intersection matrix. This is topological
\\ data, not analytic!
K(m) =
{
	vector(matsize(m)[1],i,-2-m[i,i])*m^-1;
}

delta(II) =
{
    local(n,i,j);
    n=matsize(II)[1];
    vector(n,i,sum(j=1,n,II[i,j])-II[i,i]);
}

\\ Returns a submatrix for the vector. Isn't there a standard function
\\ that does this in gp/pari?
\\ This doesn't really error check very much, so make sure to put in some
\\ sensible input.
submatrix(m,v) =
{
	local(r,l,i,j);
	l = matsize(v)[2];
	if(matsize(m)[1] < l || matsize(m)[2] < l,\
		print("This matrix is to small!");
		return [0];
	);
	r = matrix(l,l);
	for(i=1, l,\
		for(j=1, l,\
			r[i,j] = m[v[i],v[j]];
		);
	);
	return(r);
}

submatrix(m,v,w) =
{
    local(r,l,k,i,j);
    l = matsize(v)[2];
    k = matsize(w)[2];
    if(matsize(m)[1] < l || matsize(m)[2] < k,\
        print("This matrix is to small!");
        return [0];
    );
    r = matrix(l,k);
    for(i=1, l,\
        for(j=1, k,\
            r[i,j] = m[v[i],w[j]];
        );
    );
    return(r);
}

subvector(m,v) =
{
    local(r,l,i);
    l = matsize(v)[2];
    return(vector(l, i, m[v[i]]));
}

\\ Returns the branch in m of C containing the neighbour D.
\\ Again, sensible input is needed.
branch(m,C,D) =
{
	local(V,w,n,a,i);
	n = matsize(m)[1];
	V = vector(n,i,0);
	V[D] = 1;
	a = 1;
	while(a,
		a = 0;
		for(i=1, n,\
			for(j=1, n,\
				if(m[i,j] == 1 && V[i] == 1 && V[j] == 0 && j != C,\
					V[j] = 1;
					a = 1;
				);
			);
		);
	);
	w = [];
	for(i=1,n,
		if(V[i],
			w = concat(w,[i]);
		);
	);
	return(submatrix(m,w));
}

\\ Gives the neighbours of n wrt the intersection matrix m
neighbours(m,n) =
{
	local(i,r);
	r = [];
	for(i=1,matsize(m)[1],\
		if(m[n,i] == 1,\
			r = concat(r,[i]);
		);
	);
	return(r);
}

\\ Gives a list with all the ends of a graph, given the intersection matrix
ends(m) =
{
	local(e,n,D,i);
	e = [];
	n = matsize(m)[1];
	D = delta(m);
	for(i=1,n,\
		if(D[i]==1,\
			e = concat(e,[i]);
		);
	);
	e;
}

\\ Gives a list with all the nodes of a graph, given the intersection matrix
nodes(m) =
{
	local(s,D);
	s = [];
	n = matsize(m)[1];
	D = delta(m);
	for(i=1,n,\
		if(D[i]>2,\
			s = concat(s,[i]);
		);
	);
	s;
}

\\ Gives a list with all the leg-determinants of a graph, given the
\\ intersection matrix
alphas(m) =
{
    local(e,l,D,a,i,v,k,vp,j);
	e = ends(m);
	l = matsize(e)[2];
	D = delta(m);
	a = [];
	for(i=1,l,\
		v = e[i];
		k = [];
		vp = 0;
		while(D[v] <= 2,\
			k = concat(k,m[v,v]);
			j = 1;
			while(m[v,j] <= 0 && j != vp, j++);
			vp = v;
			v = j;
		);
		a = concat(a,[abs(numerator(incf(k)))]);
	);
	a;
}

seiferts(m) =
{
	local(e,l,D,i,v,k,vp,j,S,r);
	e = ends(m);
	l = matsize(e)[2];
	D = delta(m);
	S = [];
	for(i=1,l,\
		v = e[i];
		k = [];
		vp = 0;
		while(D[v] <= 2,\
			k = concat(k,m[v,v]);
			j = 1;
			while(m[v,j] <= 0 && j != vp, j++);
			vp = v;
			v = j;
		);
		r = incf(k);
		S = concat(S,[[abs(numerator(r)),abs(denominator(r))]]);
	);
	S;
}

is_integral(v) =
{
	r = 1;
	for(i=1,matsize(v)[2],\
		if(denominator(v[i]) != 1,
			r = 0;
		);
	);
	r;
}

\\ The following function calculates the geometric genus of a Newton
\\ nondegenerate hypersurface. x is a list of the normal vectors of the
\\ faces of the diagram and m a list of the corresponding values of the normal
\\ vectors. All it does is count the points under the diagram.
pg(x,m) =
{
	local(no,o,i,j,k,p,l,c,du);
	o = [0,0,0];
	no = matsize(x)[2];
	for(i=1,3,\
		j = 1;
		p = [1,1,1];
		while(o[i] == 0,\
			o[i] = j;
			p[i] = j;
			for(k=1, no,\
				if(x[k] * p~ < m[k],\
					o[i] = 0;
				);
			);
			j++;
		);
	);
	l = o[1]*o[2]*o[3];
	c = 0;
	for(i=0, l-1,\
		p = numvec(o,i) + [1,1,1];
		du = 0;
		for(j=1, no,\
			if(x[j] * p~ <= m[j],\
				du = 1;
			);
		);
		if(du,\
			c++
		);
	);
	return(c);
}

\\ This one also takes into account some points which appear, but shouldn't
pg_fake(x,m) =
{
	local(no,o,i,j,k,p,l,c,du);
	o = [0,0,0];
	no = matsize(x)[2];
	for(i=1,3,\
		j = 1;
		p = [1,1,1];
		while(o[i] == 0,\
			o[i] = j;
			p[i] = j;
			for(k=1, no,\
				if(x[k] * p~ < m[k],\
					o[i] = 0;
				);
			);
			j++;
		);
	);
	o[1]++;
	o[2]++;
	o[3]++;
	l = o[1]*o[2]*o[3];
	c = 0;
	for(i=0, l-1,\
		p = numvec(o,i);
		if(p[1]*p[2]*p[3] == 0,\
			du = 0;
			for(j=1, no,\
				if(x[j] * p~ <= m[j],\
					du = 1;
				);
			);
			for(j=1, no,\
				if(x[j] * p~ < x[j] * [1,1,1]~,\
					du = 0;
				);
			);
			if(du,\
				c++
			);
		);
	);
	return(c);
}

numvec(o,j) =
{
	local(v,i);
	v = vector(matsize(o)[2]);
	for(i=1, matsize(o)[2],\
		if(o[i],\
			v[i] = j%o[i];
			j = floor(j/o[i]);
		);
	);
	v;
}

pgpg(x,m) =
{
	local(m,i,j,k,h,o,q,t,no,p);
	no = matsize(x)[2];
	o=[1,1,1];
	for(i=1,3,\
		j = 1;
		t = 1;
		q = [1,1,1];
		while(t,\
			t = 0;
			for(k=1,no,\
				if(x[k]*q~ <= m[k],\
					t=1;
				);
			);
			q[i] = q[i] + 1;
		);
		o[i] = q[i];
	);

	p = 0;

	for(i=1,o[1],\
	for(j=1,o[2],\
	for(k=1,o[3],\
		t = 1;
		for(h=1,no,\
			if(x[h]*[i,j,k]~ > m[h],\
				t = 0;
			);
		);
		if(t,\
			p++;
		);
	);
	);
	);

	return(p);
}

\\ The follwoing is a brute-force attempt at listing all the elements of the
\\ group H.
orders(m) =
{
	local(e,o,i,j,k,is_not,mi,s,f,v,a,d);
	f = [];
	s = 1;
	mi = m^-1;
	e = ends(m);
	E = vector(matsize(e)[2]);
	for(i=1, matsize(e)[2],\
		E[i] = vector(matsize(m)[1], j, mi[e[i],j]);
	);
	o = vector(matsize(e)[2]);
	for(i=1,matsize(e)[2],\
		k=1;
		d = 1;
		while(d,\
			j = 0;
			v = numvec(o,j);
			while(!is_integral(sum(a=1,i-1,v[a]*E[a]) - k*E[i]) & j < s,\
				j++;
				v = numvec(o,j);
			);
			if(j < s,
				o[i] = k;
				d = 0;
				s = s*k;
			);
			k++;
		);
	);
	o;
}

\\ The Casson-Walker of a negative definite plumbed three-manifold.
CW(m) =
{
	local(D,mi);
	D = delta(m);
	mi = m^-1;
	-matdet(-m)/24 *\
	(\
		sum(i=1,matsize(m)[1], m[i,i]) +\
		3*matsize(m)[1] +\
		sum(i=1,matsize(m)[2], (2-D[i])*mi[i,i])\
	);
}

R(m,mi,co) =
{
	local(i,j,nco,res);
	i=0;
	j=0;
	res = [];
	a = alphas(m);
	e = ends(m);
	lv = matsize(co)[2];
	if(lv < matsize(e)[2],\
		for(i=0, a[lv+1]-1,\
			nco = concat(co,[i]);
			res = concat(res,R(m,mi,nco));
		);
		return(res);
	);
	if(lv == matsize(e)[2],\
		mi = m^-1;
		cand = vector(matsize(m)[1],i,\
			sum(j=1,matsize(co)[2],\
				co[j]*mi[e[j],i];
			);
		);
		if(is_integral(cand),\
			res = [cand];
			print(co);
			write(tmp_output,cand);
			return([cand]);
		);
	);
	res;
}

\\ analytic euler characteristic of a cycle on a manifold given by a graph
chi(Z,m) =
{
	return(-1/2*Z*m*(K(m)+Z)~)
}

sg(v,m) =
{
	local(i,j);
	for(i=0, m/v[1],\
		for(j=0, (m-i*v[1])/v[2],\
			if((m-i*v[1]-j*v[2]) % v[3] == 0,\
				print([i,j,(m-i*v[1]-j*v[2])/v[3]])
			);
		);
	);
}

meet(S) =
{
	local(n, s, i, j, w);
	n = matsize(S)[2];
	s = matsize(S[1])[2];
	w = S[1];
	for(i=2, n,\
		for(j=1, s,\
			if(S[i][j] < w[j],\
				w[j] = S[i][j];
			);
		);
	);
	return(w);
}

join(S) =
{
	local(n, s, i, j, w);
	n = matsize(S)[2];
	s = matsize(S[1])[2];
	w = S[1];
	for(i=2, n,\
		for(j=1, s,\
			if(S[i][j] > w[j],\
				w[j] = S[i][j];
			);
		);
	);
	return(w);
}

simplex(i,o) =
{
	local(w,X,Y,Z);
	w = o[2][i];
	X = vector(matsize(o[1])[1], j, o[2][j][1]);
	Y = vector(matsize(o[1])[1], j, o[2][j][2]);
	Z = vector(matsize(o[1])[1], j, o[2][j][3]);
	return(meet([w[2]*w[3]*X, w[1]*w[3]*Y, w[1]*w[2]*Z]));
}

\\ The following function takes the output of the above function Oka() and
\\ writes out a picture of the resolution graph onto a "projective plane of
\\ normal vectors".
draw(o) =
{
	local(n);
	n = matsize(o[1])[1];
	write(output, "#FIG 3.2  Produced by some gp script");
	write(output, "Landscape");
	write(output, "Center");
	write(output, "Metric");
	write(output, "A4");
	write(output, "100.00");
	write(output, "Single");
	write(output, "-2");
	write(output, "1200 2");
	for(i=1,n,\
		for(j=1, i-1,\
			if(o[1][i,j] == 1,\
				write(output, "2 1 0 1 0 7 50 -1 -1 0.000 0 0 -1 1 1 2");
				write(output, "        13 1 1.00 60.00 120.00");
				write(output, "        13 1 1.00 60.00 120.00");
				write1(output, "         ");
				write1(output, floor(o[2][i][1]/o[2][i][3]*30000));
				write1(output, " ");
				write1(output, floor(o[2][i][2]/o[2][i][3]*30000));
				write1(output, " ");
				write1(output, floor(o[2][j][1]/o[2][j][3]*30000));
				write1(output, " ");
				write(output, floor(o[2][j][2]/o[2][j][3]*30000));
			);
		);
	);

}

\\ The Fourier-transform of the Reidemeister-Turaev torsion of a plumbed
\\ negative definite three manifold with the canonical spin^c structure
\\ See e.g. Nemeti-Nicolaescu for the general formula.
TTh(m) =
{
	local(o,d,i,j,k,v,R,nf,mi,w,a,c,n,Ev,si);
	mi = m^-1;
	o = orders(m);
	e = ends(m);
	d = matdet(-m);
	D = delta(m);
	R = vector(d-1);
	nf = nfinit(polcyclo(d,xi));
	for(i=1,d-1,\
		v = numvec(o,i);
		c = vector(matsize(m)[2], k,\
			sum(a=1, matsize(e)[2], d*v[a]*mi[e[a],k]));
		n = 1;
		while(c[n] == 0, n++);
		Ev = vector(matsize(m)[1], a, d*mi[n,a]);
		R[i] = 1;
		si=0;
		for(j=1, matsize(m)[1],\
			if(D[j] > 2,\
				if(c[j]%d ==  0,\
					R[i] = nfeltmul(nf, R[i], Ev[j]^(D[j]-2));
					si = si + D[j]-2;
				);
				if(c[j]%d !=  0,\
					R[i] = nfeltmul(nf, R[i], (xi^(c[j]%d) - 1)^(D[j]-2));
				);
			);
			if(D[j] == 1,\
				if(c[j]%d ==  0,\
					R[i] = nfeltdiv(nf, R[i], Ev[j]);
					si--;
				);
				if(c[j]%d !=  0,\
					R[i] = nfeltdiv(nf, R[i], xi^(c[j]%d) - 1);
				);
			);
		);
		if(si < 0,\
			printf("Turaev torsion is not calculated correctly! si=%d.\n", si);
		);
		if(si > 0,\
			R[i] = 0;
		);
	);
	[R,nf];
}

\\ The Reidemeister-Turaev torsion of a plumbed
\\ negative definite three manifold with the canonical spin^c structure
TT(m) =
{
	if(matdet(-m) == 1, return(0));
	local(i,h,r);
	r = 0;
	h = TTh(m);
\\	printf("matdet(-m)-1: %d",matdet(-m)-1);
\\	for(i=1, matdet(-m)-1,\
\\		r = nfeltadd(h[2], r, h[1][i]);
\\	);
\\	r[1]/matdet(-m);
	sum(i=1,matdet(-m)-1, h[1][i])[1]/matdet(-m);
}


\\ The Seiberg-Witten invariant of the link
SW(m) =
{
	local(i,t);
	-1/matdet(-m) * CW(m) + TT(m);
}

\\ The normalized Seiberg-Witten invariant of the link
SWn(m) =
{
	SW(m) - (K(m)*m*K(m)~ + matsize(m)[1])/8;
}


G(m) =
{
	local(d,v,i,j,k,n,o,R,mi);
	mi = m^-1;
	n = matsize(m)[1];
	o = vector(n,i,1);
	R = [];
	for(i=1,n,\
		for(j=1,n,\
			o[i] = lcm(o[i], denominator(mi[i,j]));
		);
		R = concat(R,[vector(n,k,o[i]*mi[i,k])]);
	);
	print(mi);
	d = prod(i=1,n,o[i]);
	for(i=1, d-1,\
		v = numvec(o,i)*mi;
		if(is_integral(v),\
	\\		print([v,L1(v)]);
			R = concat(R,[v]);
		);
	);
	return(R);
}

L1(v) =
{
	local(i);
	sum(i=1,matsize(v)[2], abs(v[i]));
}


hg(g,J) =
{
	local(R,i,j,p);
	R = [];
	T = [];
	for(i=1, matsize(g)[2],\
		for(j=1, matsize(J)[2],\
			p = g[i]+J[j];
			if(is_on_list(p,T),\
				if(!is_on_list(p,R),\
					R = concat(R,[p]);
				),
				T = concat(T,[p]);
			);
		);
	);
	return(R);
}

is_on_list(p,T) =
{
	local(i);
	for(i=1, matsize(T)[2],\
		if(p == T[i],
			return(1);
		);
	);
	return(0);
}


/*
	The following functions have to do with the geometry of rational
	projective space
*/

segm(a,b) =
{
	local(A,B);
	A = [numerator(a),0,numerator(a)-denominator(a)];
	B = [0,numerator(b),numerator(b)-denominator(b)];
	return(bamboo(A,B)[3]);
}



/*
	
*/

nstar(m,v) =
{
	local(n,i,R,s);
	n = -m^-1;
	R = [m[v,v]];
	for(i=1,matsize(m)[1],\
		if(m[v,i] == 1,
			s = matsize(R)[1];
			R = matconcat([R,0;0,block(n[v,v]/n[v,i])]);
			R[1,s+1] = 1;
			R[s+1,1] = 1;
		);
	);
	return(R);
}


/*
	The following functions calculate the SW invariant of a graph using
	Braun and Nemethi's surgery formula.
*/

SWn_ferm(m) =
{
	local(n,i,j,d,ns,es,or,del,mi,ind,f,a,b,p,pc,sw,nb);
	d = matdet(-m);
	if(is_rat(m),\
		return(0)\
	);

\\	if(matsize(nodes(m))[2] == 1, return pg_P
	
	n = nodes(m)[1];

	system("rm tmp tmpf tmp.out");
	d = matdet(-m);
	del = delta(m);
	ns = nodes(m);
	es = ends(m);
	or = orders(m);
	mi = -d*m^-1;
	ntor = [];
	ntes = [];
	for(i=1,matsize(or)[2],\
		if(or[i] != 1,
			ntor = concat(ntor,[or[i]]);
			ntes = concat(ntes,[es[i]]);
		);
	);


	write(tmp, "&(J = xi);");
	write1(tmp, "&(P=");
	write1(tmp, polcyclo(d,xi));
	write(tmp, ",1);");

	write(tmp, "&(J = t);");
	write(tmp, "&(S = 'tmp.out');");

	write1(tmp, "f := 1/", d, "*");

	ind = ["i","j","k","l","m","n","o","p"];
	for(i=1,matsize(ntor)[2],\
		write(tmp, "Sigma <", ind[i] ," = 0, ", ntor[i]-1, "> [");
	);

	for(i=1,matsize(ns)[2],\
		write1(tmp, "	(1-xi^((");
		for(j=1,matsize(ntor)[2],\
			if(j!=1,\
				write1(tmp, "+")
			);
			write1(tmp, ind[j], "*", mi[ntes[j],ns[i]])
		);
		write(tmp, ")|", d, ") * t^", mi[n,ns[i]], ")^",del[ns[i]]-2,"*");

	);

	for(i=1,matsize(es)[2],\
		write1(tmp, "	(1-xi^(");
		for(j=1,matsize(ntor)[2],\
			if(j!=1,\
				write1(tmp, "+")
			);
			write1(tmp, ind[j], "*", mi[ntes[j],es[i]])
		);
		write1(tmp, ") * t^", mi[n,es[i]], ")^-1");

		if(i!=matsize(es)[2],
			write1(tmp,"*");
		);
		write(tmp,"");
	);


	for(i=1,matsize(ntor)[2],\
		write1(tmp, "]");
	);
	write(tmp, ";");
	write(tmp, "&s;");
	write(tmp, "&q;");
	

	system("~/Fermat/fermat/ferl");

	system("cat tmp.out |
        awk 'BEGIN {x=0}
        {
            if($1 == \"f\")
            {
                x = 1;
                s = \"asdf\";
            }
            if(x)
            {
                s = s $0;
            }
            if(x && substr($0,length($0),1) == \";\")
            {
                print substr(s,14,length(s)-14);
                exit;
            }
        }' | sed s/\\\\\\(\\[0-9\\]\\\\\\)t/\\\\1\\*t/g > tmpf");

	f = read("tmpf");

	a = numerator(f);
	b = denominator(f);
	p = (a-(a%b))/b;
	pc = subst(p,t,1);
	nb = neighbours(m,n);
	printf("pc = %d\n", pc);
	sw = pc + sum(i=1,matsize(nb)[2], SWn_ferm(branch(m,n,nb[i])));
	return(sw);
}

/*
	Some functions which calculate auxiliary primes for Fermat
*/

phi_hasroot(n,l) =
{
	local(r,k);
	r = [];

	while(matsize(r)[2] < l,\
		if(isprime(n*k+1),\
			r = concat(r,[n*k+1]);
		);
		k++;
	);

	return(r);
}

phi_irred(n,l) =
{
	local(r,p);
	r = [];
	p = 2;
	while(matsize(r)[2] < l,\
		if(isprime(p) && gcd(n,p-1) == 1,
			r = concat(r,[p]);
			print(p);
		);
		p = nextprime(p+1);
	);
	return(r);
}



/*
	Some functions for seifert invariants and Pinkham-Dolgachev theory
*/


\\ Calculates the geometric genus of a singularity with a good C* action
\\ whose central vertex is rational with self intersection number b
\\ and Seifert invariants S.
pg_PD(b,S) =
{
	local(i,j,nu,l,r,bnd,ch);
	nu = matsize(S)[2];
	
	ch = 1 - sum(i=1,nu,(S[i][1]-1)/S[i][1]);
	e = b + sum(i=1,nu,S[i][2]/S[i][1]);

	bnd = floor(ch/e);
	printf("bnd = %d\n",bnd);
	\\ Replace this!
	r = sum(l=0,bnd,
		max(0,l*b + sum(j=1,nu,ceil(l*S[j][2]/S[j][1])) - 1 )  );
	return(r);
}

inspec_PD(b,S) =
{
	local(i,j,nu,l,r,bnd,ch);
	nu = matsize(S)[2];
	
	ch = 1 - sum(i=1,nu,(S[i][1]-1)/S[i][1]);
	e = b + sum(i=1,nu,S[i][2]/S[i][1]);

	bnd = floor(ch/e);
	printf("bnd = %d\n",bnd);
	\\ Replace this!
	r = sum(l=0,bnd,
		max(0,l*b + sum(j=1,nu,ceil(l*S[j][2]/S[j][1])) - 1 ) * t^l  );
	return(r);
}


/*
	polynomial and rational parts of series in one variable
*/

pol_part(P) = 
{
	local(a,b);
	a = numerator(P);
	b = denominator(P);
	return((a-(a%b))/b);
}

rat_part(P) =
{
	local(a,b);
	a = numerator(P);
	b = denominator(P);
	return(P-(a-(a%b))/b);
}



/*
  An implementation of the computation sequence
*/

c_laufer(Z,m) =
{
	local(n,i,j,dum,dumm,N,del,Zl);
	del = delta(m);
	n = nodes(m);
	S = matsize(m)[1];
	N = matsize(n)[2];
	compl = vector(S-N);
	Zl = -K(o[1]) - vector(S,i,1);

	j = 1;
	for(i=1,S,\
		if(del[i] <= 2,\
			compl[j] = i;
			j++;
		);
	);
	

	dum = 1;
	while(dum,\
		i=1;
		dumm = 1;
		while(dumm && i<=S-N,\
			if((Z*m)[compl[i]] > (2-del[compl[i]])*(Z[compl[i]]/Zl[compl[i]]),\
				Z[compl[i]]++;
				dumm = 0;
			);
			i++;
		);
		if(dumm,
			dum = 0;
		);
	);
	return(Z);
}

x_laufer(Z,m) =
{
	local(n,i,j,dum,dumm,N,del,Zl);
	del = delta(m);
	n = nodes(m);
	S = matsize(m)[1];
	N = matsize(n)[2];
	compl = vector(S-N);

	j = 1;
	for(i=1,S,\
		if(del[i] <= 2,\
			compl[j] = i;
			j++;
		);
	);
	

	dum = 1;
	while(dum,\
		i=1;
		dumm = 1;
		while(dumm && i<=S-N,\
			if((Z*m)[compl[i]] > 0,\
				Z[compl[i]]++;
				dumm = 0;
			);
			i++;
		);
		if(dumm,
			dum = 0;
		);
	);
	return(Z);
}

comp_seq_article(m) =
{
	local(n,i,j,dum,dumm,N,del,Zl,mi,ma,ZZ,co,Zll);
	del = delta(m);
	n = nodes(m);
	S = matsize(m)[1];
	N = matsize(n)[2];
	
	Zl = -K(m) - vector(S,i,1);
	Z = vector(S);
	ZZ = [];
	co = [];

	Zll = vector(S);
	for(i=1,N,\
		Zll[n[i]] = -K(m)[n[i]];
	);
	Zll = c_laufer(Zll,m);

	
	while(Z != Zll,
		mi = 1;
		for(i=1,N,\
			if(Z[n[i]]/Zl[n[i]] < mi,
				mi = Z[n[i]]/Zl[n[i]];
				j = i;
				ma = (Z*m)[n[i]]
			);
			if(Z[n[i]]/Zl[n[i]] == mi,
				if( (Z*m)[n[i]] > ma,\
					ma = (Z*m)[n[i]];
					j = i;
				);
			);
		);
		co = concat(co,[max(0, (-Z*m)[n[j]] + 1)]);
		ZZ = concat(ZZ,[Z]);
		Z[n[j]]++;
		Z = c_laufer(Z,m);
	);
	return([ZZ,co]);
}


\\ This is a shorthand for a few steps:

pg_csa(m) =
{
	local(e,i);
	e = comp_seq_article(m);
	return(sum(i=1,matsize(e[2])[2], e[2][i]));
}

lattice_example_p(p) =
{
	local(f,o,d,l,x,i,j,ZK);
	if(p%6==0,\
		print("p must not be divisible by 6");
		return(0);
	);
	d = [0,1;1,0];
	l = [ [ [1,0,0],[0,1,0] ], [  ] ];
	for(i=1,gcd(p,3),\
		l[2] = concat(l[2],[[-1,0,1]]);
	);
	for(i=1,gcd(p,2),\
		l[2] = concat(l[2],[[0,-1,1]]);
	);
	x = [ [21,14,6], [-3*p, -2*p, 5*p+6]/content([-3*p, -2*p, 5*p+6]) ];
	o = Oka(d,l,x);
	ZK = -K(o[1]);
	n = matsize(o[1])[1];

	f = concat("lattice_example_p_", p);
	write(f, concat("in", n)); 
	write(f, "iv2");
	write(f, "ib 0 1");
	write(f, concat(concat("iK ", floor(ZK[1])),concat(" ",floor(ZK[2]))));
	write(f, "iI");
	for(i=1,n,\
		for(j=1,n,\
			write1(f,concat(o[1][i,j]," "));
		);
		write(f,"");
	);
	write(f, "c");
	write(f, "pc");
	return(o);
}

\\ returns 1 if the vectors generate the 1-dimensional faces of a strictly
\\ convex cone, in this order. 0 otherwise.

is_good_cone(x) =
{
	local(i,j,n);
	n = length(x);
	for(i=1,n,\
		for(j=i+2,i+n-1,\
			if(cross(x[i],x[i%n+1])*x[(j-1)%n+1]~ <= 0,
				return(0);
			);
		);
	);
	return(1);
}

is_good_cone_insides(x,y) =
{
	local(n,m,i,j);
	if(!is_good_cone(x),
		return(0);
	);
	n = length(x);
	m = length(y);
	for(i=1,n,\
		for(j=1,m,\
			if(cross(x[i],x[i%n+1])*y[j]~ <= 0,
				return(0);
			);
		);
	);
	return(1);
}





/*
	The follwoing blocks of data can be used as input for the function Oka()
*/

\\d = [\
\\0, 1;\
\\1, 0];
\\x = [ [6,9,7],[9,6,8] ];
\\l = [ \
\\	[ [0,0,1], [1,5,0]],\
\\	[ [1,0,0], [0,0,1]]];

\\d = [\
\\0, 1;\
\\1, 0];
\\x = [ [6,10,21],[2,2,5] ];
\\l = [ \
\\	[ [0,1,0], [0,0,1]],\
\\	[ [0,0,1], [2,0,1]] ];

\\d = [\
\\0, 1;\
\\1, 0];
\\x = [[907283, 800110, 8989],[707, 700, 1111]];
\\l = [ \
\\	[ [1,0,0], [0,1,0]],\
\\	[ [0,0,1], [0,1,0]]];

\\d = [\
\\0,1,0;\
\\1,0,1;\
\\0,1,0];
\\x = [ [12,8,23], [12,5,20], [175,63,286] ];
\\l = [\
\\	[ [0,0,1], [0,4,1] ],\
\\	[ [0,1,0] ],\
\\	[ [0,0,1], [7,0,1] ] ];

\\d = [\
\\0,1,1;\
\\1,0,0;\
\\1,0,0];
\\x = [\
\\	[15, 35, 43],\
\\	[6,5,10],\
\\	[48,112,21] ];
\\l = [\
\\	[ [0,5,1], [0,0,1], [0,0,1] ],\
\\	[ [1,0,5], [1,0,0], [1,0,0], [1,0,0], [1,0,0] ],\
\\	[ [1,0,0], [0,1,0] ] ];

\\d = [\
\\0, 1;\
\\1, 0];
\\x = [ [6,10,15],[3,5,8] ];
\\l = [ \
\\	[ [1,0,0], [0,1,0]],\
\\	[ [1,0,1], [0,1,4]]];

\\ a moving triangle given by
\\ f = x5z + y3z + x4y2 + z5
\\ d = [\
\\0, 1;\
\\1, 0];
\\x = [ [12,20,15],[3,5,7] ];
\\l = [ \
\\	[ [1,0,0], [0,1,0]],\
\\	[ [1,0,4], [0,1,2]]];
\\m = [75, 22];

d = [\
0, 1;\
1, 0];
x = [ [400,561,110],[109,154,31] ];
l = [ \
	[ [1, 0,0], [0,1,0]],\
	[ [0,12,1], [1,0,13] \
] ];

\\d = [\
\\1];
\\x = [ [3*19,2*19,2*3] ];
\\l = [ \
\\	[ [1,0,0], [0,1,0], [0,0,1] ]\
\\];


\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0];
\\x = [\
\\  [25, 31, 23],\
\\  [42, 77, 60],\
\\  [15, 12, 16],\
\\  [56, 60, 35] \
\\];
\\l = [ \
\\	[                  ],\
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,0,1], [1,0,0] ],\
\\	[ [1,0,0], [0,1,0] ] \
\\];
\\m = [286, 630, 132, 525];

\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0];
\\x = [\
\\  [33, 15, 32],\
\\  [39, 24, 65],\
\\  [15,  6, 14],\
\\  [66, 30, 55] \
\\];
\\l = [ \
\\	[                  ],\
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,0,1], [1,0,0] ],\
\\	[ [1,0,0], [0,1,0] ] \
\\];

\\ the one with group of order 47:
\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0];
\\x = [\
\\  [11,  5,  7],\
\\  [ 6,  3,  4],\
\\  [32, 12, 21],\
\\  [15,  8,  6] \
\\];
\\l = [ \
\\	[                  ],\
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,0,1], [1,0,0] ],\
\\	[ [1,0,0], [0,1,0] ] \
\\];
\\
\\d = [\
\\0, 1, 1;\
\\1, 0, 1;\
\\1, 1, 0];
\\x = [\
\\  [14, -3, -3],\
\\  [ 0, -2,  5],\
\\  [ 0,  5, -2] \
\\];
\\l = [ \
\\	[ [ 4,-1,-1], [ 4,-1,-1], [ 4,-1,-1],\
\\	  [ 4,-1,-1], [ 4,-1,-1], [ 4,-1,-1] ],\
\\	[ [-1,-1, 3], [-1,-1, 3] ],\
\\	[ [-1, 3,-1], [-1, 3,-1] ] \
\\];

\\ f = x6 + x2y3 + y6 + xz7
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [21, 28, 15],\
\\  [21, 14,  9]\
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [0,0,1], [6,1,0] ] \
\\];
\\m = [126, 84];

\\ f = x13 + y13 + x2y2 + z3 = 0
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [  6, 33, 26],\
\\  [ 33,  6, 26] \
\\];
\\l = [ \
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [78, 78];

\\ the example from the poincare series article
\\ f = x3 + y2x2 + y7 + yz2
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [ 4,  2,  5],\
\\  [ 5,  2,  6]\
\\];
\\l = [ \
\\	[ [1,3,0], [0,0,1] ],\
\\	[ [1,0,2], [1,0,0] ] \
\\];
\\m = [12, 14];

\\ f = x7 + x2y2 + y5 + x2z3 + yz3
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [ 6, 15, 10],\
\\  [ 3,  6,  4],\
\\  [ 9,  6,  8] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [1,2,0]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [42, 18, 30];

\\ f = x23 + x3y3 + y7 + x4z4 + z5
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [ 12, 80, 57 ],\
\\  [  3, 17, 12 ],\
\\  [ 20, 15, 21 ]\
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [0,1,0]          ],\
\\	[ [1, 0, 0], [0,0,1] ] \
\\];
\\m = [276, 60, 105];

\\ f = x26 + x4y4 + y26 + z^7
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [  6,  9, 10],\
\\  [  3,  3,  4] \
\\];
\\l = [ \
\\	[ [0,1,0], [0,0,1]          ],\
\\	[ [1,0,0], [0,0,1], [0,0,1] ] \
\\];
\\m = [30, 12];

\\ f = x4 + x2y2 + y6 + x3z3 + y5z3 + z49
\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0 \
\\];
\\x = [\
\\  [  15,   9,  1],\
\\  [   3,   3,  1],\
\\  [   6,   3,  1],\
\\  [ 230, 138, 15] \
\\];
\\l = [ \
\\	[                           ],\
\\	[ [0,1,0], [0,0,1], [0,0,1] ],\
\\	[ [1,0,0], [0,0,1], [0,0,1] ],\
\\	[ [1,0,0], [0,1,0]          ] \
\\];
\\m = [48, 24, 36, 735];

\\ f = x6 + x5y3 + y109 + x3z3 + y3z7 + z22
\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0 \
\\];
\\x = [\
\\  [  21,  1,  15],\
\\  [   3,  1,   3],\
\\  [ 742, 35, 530],\
\\  [  19, 15,   3] \
\\];
\\l = [ \
\\	[                                    ],\
\\	[ [0,0,1], [0,1,0], [0,1,0], [0,1,0] ],\
\\	[ [1,0,0], [0,0,1]                   ],\
\\	[ [1,0,0], [1,0,0], [1,0,0], [0,1,0] ] \
\\];
\\m = [108, 18, 3815, 66];

\\ f = x6 + x5y2 + y101 + x2z5 + y101z7 + z9
\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0 \
\\];
\\x = [\
\\  [  509,  4,  307 ],\
\\  [   10,  5,    8 ],\
\\  [ 4459, 35, 2690 ],\
\\  [  505,  6,  202 ] \
\\];
\\l = [ \
\\	[                  ],\
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,0,1], [1,0,0] ],\
\\	[ [1,0,0], [0,1,0] ] \
\\];
\\m = [2553, 60, 22365, 2020];


\\ There is something wrong with this example:
\\d = [\
\\0, 1, 1;\
\\1, 0, 0;\ 
\\0, 1, 0];
\\x = [\
\\  [11, 5, 7],\
\\  [ 6, 3, 4],\
\\  [32,12,21] \
\\];
\\l = [ \
\\	[ [9,8,-6]            ],\
\\	[ [0,1,0],    [0,0,1] ],\
\\	[ [0,0,1],    [1,0,0] ] \
\\];


\\ Suspension:
\\ f = x8 + x4y3 + x2y6 + y11 + z5
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [15, 20, 24],\
\\  [15, 10, 18],\
\\  [25, 10, 22] \
\\];
\\l = [ \
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,0,1]          ],\
\\	[ [0,0,1], [1,0,0] ] \
\\];
\\m = [120, 90, 110];


\\ This is not a suspension:
\\ f = x7 + x2y2 + y9 + x3y3 + y5z3 + z7
\\d = [\
\\0, 1, 0, 0;\
\\1, 0, 1, 0;\
\\0, 1, 0, 1;\
\\0, 0, 1, 0];
\\x = [\
\\  [ 6, 15,  8],\
\\  [ 8, 13,  6],\
\\  [27,  8, 10],\
\\  [21,  6,  8] \
\\];
\\l = [ \
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [0,1,0]          ],\
\\	[ [1,0,0]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [42,42,70,54];

\\ This is not a suspension:
\\ f = x8y + x5y2 + y5 + yz3 + z5
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [ 5, 55, 27],\
\\  [ 1, 10,  5],\
\\  [ 9, 15, 20] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,5,1] ],\
\\	[ [1,0,0]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [55,25,75];

\\ A suspension
\\ f = x8 + x4y3 + y12 + z5
\\d = [\
\\0, 1, 0, 0, 0;\
\\1, 0, 1, 0, 0;\
\\0, 1, 0, 1, 0;\
\\0, 0, 1, 0, 1;\
\\0, 0, 0, 1, 0];
\\x = [\
\\  [ 15, 20, 24],\
\\  [  3, -4,  0],\
\\  [  1, -3, -1],\
\\  [ -3,  4,  0],\
\\  [ 45, 20, 48] \
\\];
\\l = [ \
\\	[ [0,1,0], [0,0,1] ],\
\\	[                  ],\
\\	[                  ],\
\\	[                  ],\
\\	[ [0,0,1], [1,0,0] ] \
\\];
\\m = [120,0,-5,0,240];

\\ f = x11 + z13 + x5y7 + y3z7 + x2y14 + y10z3 + y21
\\d = [\
\\0, 1, 0, 0, 0;\
\\1, 0, 1, 0, 0;\
\\0, 1, 0, 1, 0;\
\\0, 0, 1, 0, 1;\
\\0, 0, 0, 1, 0];
\\x = [\
\\  [ 65, 66, 55],\
\\  [ 49, 42, 47],\
\\  [ 27, 20, 25],\
\\  [ 21,  9, 26],\
\\  [ 21,  6, 22] \
\\];
\\l = [ \
\\	[ [0,1,0], [1,0,0] ],\
\\	[ [0,0,1]          ],\
\\	[ [1,0,0]          ],\
\\	[ [0,0,1]          ],\
\\	[ [0,0,1], [1,0,0] ] \
\\];
\\m = [715, 539, 275, 168, 126];

\\ A nonbistellar exemple
\\ f = x7 + z6 + x5y5 + y13z2 + x3y12 + y26
\\d = [\
\\0, 1, 0, 0;\
\\1, 0, 1, 0;\
\\0, 1, 0, 1;\
\\0, 0, 1, 0];
\\x = [\
\\  [ 30, 12, 35],\
\\  [ 58, 20, 65],\
\\  [ 14,  4, 19],\
\\  [ 28,  6, 39] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [1,0,0]          ],\
\\	[ [0,0,1]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [210, 390, 90, 156];


\\ A similar one, just smaller.
\\ f = x7 + z3 + x4y4 + y5z2 + x3y7 + y18
\\d = [\
\\0, 1, 0, 0;\
\\1, 0, 1, 0;\
\\0, 1, 0, 1;\
\\0, 0, 1, 0];
\\x = [\
\\  [ 12,  9, 28],\
\\  [ 11,  4, 20],\
\\  [  6,  2, 11],\
\\  [ 22,  6, 39] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [1,0,0]          ],\
\\	[ [0,0,1]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [84, 60, 32, 108];

\\ f = 
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [  6, 10, 21],\
\\  [  4,  2,  7],\
\\  [  5,  2,  8] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [0,0,1], [2,1,0] ],\
\\	[ [1,0,0], [1,0,2] ] \
\\];
\\m = [];


\\ f = 
\\d = [\
\\0, 1, 1;\
\\1, 0, 0;\
\\1, 0, 0];
\\x = [\
\\  [  5,  5,  6 ],\
\\  [ 20, 15, 21 ],\
\\  [ 15, 20, 21 ] \
\\];
\\l = [ \
\\	[ [0,0,1], [1,1,0] ],\
\\	[ [1,0,0], [0,0,1] ],\
\\	[ [0,1,0], [0,0,1] ] \
\\];
\\m = [];



\\\\ f = z^28 + x21 + x7y3 + x5y4 + y8
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [ 12, 56,  9 ],\
\\  [ 28, 56, 13 ],\
\\  [ 28, 35, 10 ] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0] ],\
\\	[ [0,0,1] ],\
\\	[ [0,0,1], [1,0,0], [1,0,0], [1,0,0], [1,0,0] ] \
\\];
\\m = [252, 364, 280];

\\\\ f = x7y + x2y8 + y10 + x5z2 + y7z2 + z\\
\\d = [\
\\0, 1, 1;\
\\1, 0, 0;\
\\1, 0, 0];
\\x = [\
\\  [ 14, 10, 19 ],\
\\  [  3,  2,  4 ],\
\\  [ 28, 20, 35 ] \
\\];
\\l = [ \
\\	[ [0,2,1], [0,0,1] ],\
\\	[ [1,0,0], [1,0,2] ],\
\\	[ [0,1,0], [1,0,0] ] \
\\];
\\m = [108, 22, 210];

\\\\ f = x15,y + y22 + x5z2 + y7z2 + z3
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [ 14, 10, 75 ],\
\\  [  7,  5, 35 ] \
\\];
\\l = [ \
\\	[ [0,2,1], [0,0,1], [0,0,1], [0,0,1], [1,0,0] ],\
\\	[ [0,1,0], [1,0,0] ] \
\\];
\\m = [220, 105];


\\\\ f = x15,y + y22 + x5z2 + y7z2 + z3
\\d = [\
\\0, 1;\
\\1, 0];
\\x = [\
\\  [ 28,20,105 ],\
\\  [ 14,10, 35 ] \
\\];
\\l = [ \
\\	[ [1,0,0], [0,1,0], [0,0,1], [0,0,1], [0,0,1], [0,0,1] ],\
\\	[ [0,1,0], [1,0,0] ] \
\\];
\\m = [560, 210];


\\ f = x15,y + y22 + x5z2 + y7z2 + z3
\\d = [\
\\0, 1, 1, 1;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0;\
\\1, 0, 0, 0];
\\x = [\
\\  [ 21,15,83 ],\
\\  [ 12,15,56 ],\
\\  [ 27,12,80 ],\
\\  [ 14,10,35 ] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,0,1] ],\
\\	[ [0,1,0], [0,0,1] ],\
\\	[ [1,0,0], [0,0,1] ],\
\\	[ [0,1,0], [1,0,0] ] \
\\];
\\m = [ 354,228,324,175];

\\ This is a strictly balanced cone
d = [\
0, 1, 0;\
1, 0, 1;\
0, 1, 0];
x = [\
  [  2,  0,  1 ],\
  [  0,  0,  1 ],\
  [ -3, -3,  2 ] \
];
l = [ \
	[ [2,-9,1], [1,1,0] ],\
	[ [0,20,-1] ],\
	[ [-6,-43,4], [-17,20,-1] ] \
];

\\ This is not a balanced cone
\\d = [\
\\0, 1, 0;\
\\1, 0, 1;\
\\0, 1, 0];
\\x = [\
\\  [  2,  0,  1 ],\
\\  [  0,  0,  1 ],\
\\  [ -3, -3,  2 ] \
\\];
\\l = [ \
\\	[ [2,-9,1], 3*[1,1,0]+[2,0,1] ],\
\\	[ [0,20,-1] ],\
\\	[ [-6,-43,4], 5*[-3,-3,2]+[-17,20,-1] ] \
\\];


\\ Another suspension
x1 = [[ 323, 374, 779 ]];
l1 = [[ [0,0,1], [0,1,0],[1,-1,0] ]];
d1 = [0];
o1 = Oka(d1,l1,x1);

x2 = [[ 340, 323, 741 ]];
l2 = [[ [0,0,1], [1,0,0],[-1,1,0] ]];
d2 = [0];
o2 = Oka(d2,l2,x2);



T = [\
-2,  1,  0,  0,  0,  0,  0;\
 1, -2,  1,  0,  0,  0,  0;\
 0,  1,-12,  1,  0,  0,  0;\
 0,  0,  1, -1,  1,  1,  0;\
 0,  0,  0,  1, -2,  0,  0;\
 0,  0,  0,  1,  0, -3,  1;\
 0,  0,  0,  0,  0,  1, -2];

S = [\
-2,  1,  0,  0,  0,  0,  0,  0,  0;\
 1, -2,  1,  0,  0,  0,  0,  0,  0;\
 0,  1, -8,  1,  0,  0,  0,  0,  0;\
 0,  0,  1, -1,  1,  1,  0,  0,  0;\
 0,  0,  0,  1, -2,  0,  0,  0,  0;\
 0,  0,  0,  1,  0, -4,  1,  0,  0;\
 0,  0,  0,  0,  0,  1, -2,  1,  1;\
 0,  0,  0,  0,  0,  0,  1, -2,  0;\
 0,  0,  0,  0,  0,  0,  1,  0, -2];

Tp = [\
   -2,0,1,0,0,0,0,0,0,0,0,0;\
   0,-2,0,0,0,1,0,0,0,0,0,0;\
   1,0,-2,0,1,0,0,0,0,0,0,0;\
   0,0,0,-1,1,1,0,0,0,0,0,0;\
   0,0,1,1,-9,0,0,1,0,0,0,0;\
   0,1,0,1,0,-2,0,0,0,0,0,0;\
   0,0,0,0,0,0,-2,0,1,1,0,0;\
   0,0,0,0,1,0,0,-2,0,1,1,1;\
   0,0,0,0,0,0,1,0,-2,0,0,0;\
   0,0,0,0,0,0,1,1,0,-2,0,0;\
   0,0,0,0,0,0,0,1,0,0,-2,0;\
   0,0,0,0,0,0,0,1,0,0,0,-2];

LY = [\
-2,  1,  0,  0,  0,  0,  0,  0;\
 1, -1,  1,  0,  0,  1,  0,  0;\
 0,  1, -3,  1,  0,  0,  0,  0;\
 0,  0,  1, -2,  1,  0,  0,  0;\
 0,  0,  0,  1, -2,  0,  0,  0;\
 0,  1,  0,  0,  0,-15,  1,  0;\
 0,  0,  0,  0,  0,  1, -2,  1;\
 0,  0,  0,  0,  0,  0,  1, -2];

L = [\
   -4,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;\
   1,-4,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;\
   0,0,-2,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0;\
   0,0,0,-2,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0;\
   0,0,0,0,-2,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0;\
   0,0,0,0,0,-2,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0;\
   0,0,0,0,0,0,-2,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0;\
   0,0,0,0,0,0,0,-2,0,0,0,0,0,0,0,0,0,1,0,0,0,1;\
   0,0,0,0,0,0,0,0,-2,0,1,0,0,0,0,0,0,0,1,0,1,0;\
   0,0,0,0,0,0,0,0,0,-2,0,1,0,0,0,0,0,0,0,1,0,1;\
   1,0,0,0,0,0,0,0,1,0,-2,0,0,0,0,0,0,0,0,0,0,0;\
   0,1,0,0,0,0,0,0,0,1,0,-2,0,0,0,0,0,0,0,0,0,0;\
   0,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0,0,0,0,0,0;\
   0,0,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0,0,0,0,0;\
   0,0,1,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0,0,0,0;\
   0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0,0,0;\
   0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0,0;\
   0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0,0;\
   0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,-2,0,0,0;\
   0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,-2,0,0;\
   0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,-2,0;\
   0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,-2];


M = [\
-3,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0;\
 1, -1,  1,  1,  0,  0,  0,  0,  0,  0,  0;\
 0,  1, -4,  0,  0,  0,  0,  0,  0,  0,  0;\
 0,  1,  0, -3,  1,  0,  0,  0,  0,  0,  0;\
 0,  0,  0,  1, -2,  1,  0,  0,  0,  0,  0;\
 0,  0,  0,  0,  1, -7,  1,  0,  0,  0,  0;\
 0,  0,  0,  0,  0,  1, -5,  1,  0,  0,  0;\
 0,  0,  0,  0,  0,  0,  1, -1,  1,  1,  0;\
 0,  0,  0,  0,  0,  0,  0,  1, -2,  0,  0;\
 0,  0,  0,  0,  0,  0,  0,  1,  0, -4,  1;\
 0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -2]

o = Oka(d,l,x);
X = vector(matsize(o[1])[1], i, o[2][i][1]);
Y = vector(matsize(o[1])[1], i, o[2][i][2]);
Z = vector(matsize(o[1])[1], i, o[2][i][3]);
\\m = [\
\\ -2,  1,  1,  0,  1,  0,  0,  0;\
\\  1, -2,  0,  0,  0,  0,  0,  0;\
\\  1,  0, -2,  1,  0,  0,  0,  0;\
\\  0,  0,  1, -2,  0,  0,  0,  0;\
\\  1,  0,  0,  0, -2,  1,  0,  0;\
\\  0,  0,  0,  0,  1, -2,  1,  0;\
\\  0,  0,  0,  0,  0,  1, -2,  1;\
\\  0,  0,  0,  0,  0,  0,  1, -2 ];

\\m = o[1];

\\a = alphas(m);
\\e = ends(m);
\\E = [];
\\mi = m^-1;
\\print(a)
\\for(i=1,4, E = concat(E,[vector(matsize(m)[1],j,mi[e[i],j])]));
\\R = []
\\for(b=0, a[1]-1,\
\\for(c=0, a[2]-1,\
\\for(d=0, a[3]-1,\
\\for(f=0, a[4]-1,\
\\	s = b*E[1] + c*E[2] + d*E[3] + f*E[4];\
\\	if(is_integral(s),R = concat(R,[s]))\
\\)));print(b););

Pt = 
[-2,1,0,0,0,0,0,0,0,0;\
1,-1,1,1,0,0,0,0,0,0;\
0,1,-3,0,0,0,0,0,0,0;\
0,1,0,-19,1,0,0,0,0,0;\
0,0,0,1,-1,1,1,0,0,0;\
0,0,0,0,1,-2,0,0,0,0;\
0,0,0,0,1,0,-3,1,0,0;\
0,0,0,0,0,0,1,-2,1,0;\
0,0,0,0,0,0,0,1,-3,1;\
0,0,0,0,0,0,0,0,1,-2];

NN = [\
-2,1,0,0,0,0,0,0,0,0;\
1,-1,1,1,0,0,0,0,0,0;\
0,1,-3,0,0,0,0,0,0,0;\
0,1,0,-7,1,0,0,0,0,0;\
0,0,0,1,-3,1,0,0,0,0;\
0,0,0,0,1,-3,1,0,0,0;\
0,0,0,0,0,1,-7,1,0,0;\
0,0,0,0,0,0,1,-1,1,1;\
0,0,0,0,0,0,0,1,-3,0;\
0,0,0,0,0,0,0,1,0,-2];

cross_(a,b) =
{
    local(c);
    c = cross([a[1],a[2],a[3]], [b[1],b[2],b[3]]);
    return(c/content(c));
}


a = [ 12,  0, 12];
b = [  0, 13, 13];
c = [  7, -7, 15];
d = [ -7,  8, 15];
e = [-14,  0, -7];
f = [  0,-13, -6];
g = [  0,  0, 11];

x = [ cross_(b-a, g-a),\
      cross_(d-b, g-d),\
      cross_(g-a, c-a) ];

l = [ [ cross_(a,b) ],\
      [ cross_(b-f, d-f),\
        cross_(d-f, g-f) ],\
      [ cross_(g-e, c-e),\
        cross_(c-e, a-e) ] ];

ll= [ cross_(a,b)     ,\
      cross_(b-f, d-f),\
      cross_(d-f, g-f),\
      cross_(g-e, c-e),\
      cross_(c-e, a-e) ];


D = [ 0, 1, 1;\
      1, 0, 0;\
      1, 0, 0 ];

o = Oka(D,l,x);

if( ll[1] * (d-g)~ <= 0 || ll[1] * (c-g)~ <= 0,\
    print("Newton diagram not convex ..."));

for(i=0, 4,\
    j = (i-1)%5 + 1;\
    for(k=1,5,\
        if((j !=k) && (i+1 != k) && (cross(ll[j], ll[i+1]) * ll[k]~ == 0),\
            print("The cone is not convex ... " i+1 " " j " " k);\
        );\
    );\
);

compseq(d,l,x) =
{
    \\ Z is the computation sequance, i is the index
    local(o,X,L,i,j,k,Z,ZK,v,vv,Zipo,V,ints,ZKN,N);
    o = Oka(d,l,x);
\\    X = matconcat(vector(matsize(o[1])[1], i, o[2][i])~);
\\    L = matconcat(vector(matsize(l)[2], i,\
\\        matconcat(vector(length(l[i]), j, l[i][j]~)~)));
    ZK = -K(o[1]);
    ZKE = ZK;
    for(j=1, matsize(ZKE)[2], ZKE[j]--);
    Z = [ vector(matsize(o[1])[2], j, ZK[j] - floor(ZK[j])) ];
    vv = [];
    deltas = delta(o[1]);
    
    i = 1;
    N = nodes(o[1]);
    ZKN = subvector(ZK, N);
    while(cmp(subvector(Z[i],N),ZKN) == -1,\
        \\ start with the ratio test, find all minimal nodes
        V = [1];
        for(j=2, length(N),\
            if(Z[i][N[j]]/ZKE[N[j]] == Z[i][N[V[1]]]/ZKE[N[V[1]]],\
                V = concat(V,[j]);
            , if(Z[i][N[j]]/ZKE[N[j]] <  Z[i][N[V[1]]]/ZKE[N[V[1]]],\
                V = [j];
            ));
        );
        \\ next, choose one, say j, of those nodes which has maximal 
        \\ (Z_i, E_j)
        v = 1;
        ints = Z[i] * o[1];
        for(j=2,length(V),\
            if(ints[N[V[j]]] > ints[N[V[v]]],\
                v = j;
            );
        );

        Zipo = Z[i];
        Zipo[N[V[v]]]++;
        Zipo = x_nodes(Zipo,o[1]);
        Z = concat(Z, [Zipo]);
        vv = concat(vv,[N[V[v]]]);
        i++;
    );
    return([Z,vv]);
}

compseq_plhc(Z) =
{
    \\ remember that Z[1][1] is actually Z_0 and Z[2][1] is actually v(0)
    return(sum(i=1, length(Z[1])-1, max(0, (-Z[1][i]*o[1])[Z[2][i]] + 1)));
}


\\ An implementation of the generalized Laufer's algormithm, with respect
\\ to nodes. This assumes that Z <= x(Z).

x_nodes(Z, II) =
{
    local(ZZ,dummy,v,deltas,i,ints,geqz);
    deltas = delta(II);
    ZZ = Z;
    dummy = 1;
    while(dummy,\
        dummy = 0;
        ints = ZZ * II;
        for(i=1,matsize(deltas)[2],\
            if(ints[i] > 0 && deltas[i] <= 2,\
                ZZ[i]++;
                dummy = 1;
            );
        );
    );
    ZZ;
}

\\ Here: x is a matrix whose rows are vectors in \Z^3 and m is a vector.
\\ We want to find all integral points y satisfying x y \geq m, with
\\ equality in the first coordinate.
\\ this function is only for the computation sequence

\\ X = matconcat([x[1]~,matconcat(subvector(o[2],neighbours(o[1],1))~)~])
\\ M = matconcat([a,b,g])
\\ M = M*X
\\ m = vector(4,i,min(min(M[1,i], M[2,i]),M[3,i]))

find_points(x,m) =
{
    local(c,p,i,j,k,dummy,ddummy,dddummy,a,b,R,P,num_pts,selfint,s,X);
    \\ start by finding a suitable affine basis of the hyperplane
    \\ otherwise, abort :/
    i = 2;
    j = 3;
    dummy = 0;
    while(dummy == 0,\
        if(abs(matdet(submatrix(x,[1,i,j],[1,2,3]))) == 1,\
            c = [1,i,j];\
            dummy = 1;\
        ,\
            if(j < matsize(x)[2],\
                j=j+1;
            ,\
                if(i<j-1,\
                    i=i+1;
                    j=i+1;
                ,
                    dummy = 2;
                );\
            );\
        );\
    );
    if(dummy == 2,\
        print("find_points(): Oh no! The data is no good!");\
        return(0);\
    );

    \\ next, compute the number of points we expect to find,
    \\ see chapter 4 of the thesis. For this, we use the selfintersection
    \\ number associated with the vertex in question
    
    s = vector(matsize(m)[2], i, 1)~;
    s[1] = -1;
    while(x * s != [0,0,0]~,\
        s[1]--;
    );
    selfint = s[1];

    num_pts = max(0, -m * s + 1);
    

    X = submatrix(x,[1,2,3],c)~;
    p = X^-1 * subvector(m,c)~;
    a = cross([x[1,1],x[2,1],x[3,1]], [x[1,c[2]],x[2,c[2]],x[3,c[2]]]);
    b = cross([x[1,1],x[2,1],x[3,1]], [x[1,c[3]],x[2,c[3]],x[3,c[3]]]);
    if(submatrix(x,[1,2,3],[c[2]])~ * b~ == -1,\
        b = -b;\
    );
    if(submatrix(x,[1,2,3],[c[3]])~ * a~ == -1,\
        a = -a;\
    );


    i = 0;
    R = [];
    P = [0,0,0];
    while(length(R) < num_pts,\
        for(j=0, i,\
            P = p + j*a~ + (i-j)*b~;\
            dummy = 1;\
            for(k=4, matsize(m)[2],\
                if((submatrix(x,[1,2,3],[k])~ * P)[1] < 0,\
                    dummy = 0;\
                );\
            );\
            if(dummy,\
                R = matconcat([R,[P]]);
            );\
        )\ 
        i++\
    );

    return(R);
}


X = matconcat([x[1]~,matconcat(subvector(o[2],neighbours(o[1],1))~)~]);
M = matconcat([a;b;g]);
M = M*X;
m = vector(4,i,min(min(M[1,i], M[2,i]),M[3,i]));

\\Z = compseq(D,l,x);
vect_min(A) =
{
    local(i);
    if(length(A) == 1,\
        return(A[1]);
    );
    return(min(A[1], vect_min(vector(length(A)-1,i,A[i+1]))));
}

A = [a,b,c,d,e,f,g];
m = vector(length(x), i, vect_min(vector(length(A), j, A[j] * x[i]~)))
L = concat(l)
n = vector(length(L), i, vect_min(vector(length(A), j, A[j] * L[i]~)))

\\ find the points from a computation sequence, assuming that all the
\\ faces are simple triangles
find_points_simple(Z,II,x) =
{
    local(i,P,p,M,nbrs,ints);
    P = [];
    for(i=1,length(Z[2]),\
        if( (Z[1][i] * II)[Z[2][i]] == 0,\
            nbrs = neighbours(II, Z[2][i]);
            M = matconcat([x[Z[2][i]]; x[nbrs[1]]; x[nbrs[2]]]);
            p = M^-1 *\
                matconcat([\
                    x[Z[2][i]];\
                    x[nbrs[1]];\
                    x[nbrs[2]] ]);
            P = concat(P,[p]);
        );
    );
    return(P);
}


allocatemem();
allocatemem();
allocatemem();
allocatemem();

Z = compseq(D,l,x);
P = find_points_simple(Z,o[1],o[2]);


\\ A similar one, just smaller.
\\ f = x7 + z3 + x4y4 + y5z2 + x3y7 + y18
\\D = [\
\\0, 1, 0, 0;\
\\1, 0, 1, 0;\
\\0, 1, 0, 1;\
\\0, 0, 1, 0];
\\x = [\
\\  [ 12,  9, 28],\
\\  [ 11,  4, 20],\
\\  [  6,  2, 11],\
\\  [ 22,  6, 39] \
\\];
\\l = [ \
\\	[ [0,0,1], [0,1,0] ],\
\\	[ [1,0,0]          ],\
\\	[ [0,0,1]          ],\
\\	[ [1,0,0], [0,0,1] ] \
\\];
\\m = [84, 60, 32, 108];
\\
\\o = Oka(D,l,x);
\\
\\Z = compseq(D,l,x);
\\P = find_points_simple(Z,o[1],o[2]);
